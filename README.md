test2

To solve the problem of video glitching on rtsp
the garbage collection is activated using the
gc library from python

To enable it use 
gc.enable()

Also a minibuffer temporary solution is used
to help prevent the same problem, it simply
saving the current frame to show it when the
next one is recived.
