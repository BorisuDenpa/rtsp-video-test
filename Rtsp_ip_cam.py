import cv2
import time
import gc

# Enables the garbage collection
gc.enable()

# Sets the encoder for the video writing to disk
fourcc = cv2.VideoWriter_fourcc(*'XVID')
# More parameters to save the video, this must match the streaming
out = cv2.VideoWriter('output4.avi',fourcc, 24.0, (1280,720))
vcap = cv2.VideoCapture("rtsp://admin:123456@192.168.0.61:554/mpeg4")

# Flag to know if the video capture has already started
start = 0

while(vcap.isOpened()):

    ret, frame = vcap.read()
    # Displays the previous captured frame, then saves the actual
    if start == 0:
        frame1 = frame.copy()
        start = 1
    else:
        cv2.imshow('VIDEO', frame1)
        frame1 = frame.copy()
        out.write(frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

# Stops the video capture
vcap.release()
# Stops the video writing
out.release()
cv2.destroyAllWindows()